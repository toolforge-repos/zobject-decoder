{ pkgs ? import <nixpkgs> {} }:

with pkgs;

mkShell {
  buildInputs = [
    just
    php
  ];
  RUST_SRC_PATH = "${rustPlatform.rustLibSrc}";
}
