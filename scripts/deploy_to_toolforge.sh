#!/usr/bin/env bash

user=${1-$USER}

ssh $user@login.toolforge.org "
  set -ex
  become zobject-decoder bash -exc \"
    cd ~/repo &&
    git pull --ff-only &&
    rsync -a src/ ~/public_html
  \"
"
